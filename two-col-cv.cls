\ProvidesClass{two-col-cv}[2018/02/04 Custom CV Class]
\NeedsTeXFormat{LaTeX2e}

% Inherit options of article
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass[twocolumn]{article}
\setlength{\columnsep}{10pt}

\RequirePackage[
  top=0.5in,
  bottom=0.75in,
  left=0.75in,
  right=0.75in
]{geometry}

\RequirePackage{tabularx}
\RequirePackage{multirow}

\RequirePackage{enumitem}
\setlist{nosep}
\setlist[itemize]{leftmargin=*}

\RequirePackage{float}
\RequirePackage{titlesec}
\RequirePackage{ragged2e}

\RequirePackage{etoolbox}
\RequirePackage{url}
\RequirePackage{hyperref}
\hypersetup{%
	colorlinks=false
}
\setlength{\parindent}{0pt}
\pagenumbering{gobble}

\RequirePackage{customfonts}
\RequirePackage{fontawesome}

% Define custom commands
\newcommand{\email}[1]{\def\@email{#1}}
\newcommand{\phone}[1]{\def\@phone{#1}}
\newcommand{\website}[1]{\def\@website{#1}}
\newcommand{\linkedin}[1]{\def\@linkedin{#1}}
\newcommand{\github}[1]{\def\@github{#1}}
\newcommand{\gitlab}[1]{\def\@gitlab{#1}}
\newcommand{\bitbucket}[1]{\def\@bitbucket{#1}}
\newcommand{\lab}[1]{\def\@lab{#1}}
\newcommand{\labBaseUrl}[1]{\def\@labBaseUrl{#1}}
\newcommand{\faLab}[1]{\def\@faLab{#1}}

% Define maketitle
\def\@maketitle{%
  {\huge{\@author}} \\[5pt] 
  \ifdef{\@website}
  {\small{\faicon{link} \href{https://\@website}{\@website}\hfill}}
  {} 
  \ifdef{\@email}
  {\small{\faicon{envelope} \href{mailto:\@email}{\@email}\hfill}}
  {}
  \ifdef{\@phone}
  {\small{\faicon{phone} \@phone\hfill}}
  {}
  \ifdef{\@linkedin}
  {\small{\faicon{linkedin} \href{https://www.linkedin.com/in/\@linkedin}{\@linkedin}\hfill}}
  {}
  \ifdef{\@github}
  {\small{\faicon{github} \href{https://github.com/\@github}{\@github}\hfill}}
  {}
  \ifdef{\@bitbucket}
  {\small{\faicon{github} \href{https://github.com/\@github}{\@github}\hfill}}
  {}
  \ifdef{\@gitlab}
  {\small{\faicon{github} \href{https://github.com/\@github}{\@github}\hfill}}
  {}
  \ifdef{\@lab}
  {\small{\faicon{\@faLab} \href{\@labBaseUrl/\@lab}{\@lab}\hfill}}
  {}
  \vspace{1em}
}

% Format Sections
\titlespacing*{\section}{0pt}{8pt}{4pt}
\titleformat{\section}
	{\large\headerfont\raggedright}       
  	{}{0em}
  	{}
  	[\titlerule]

\titlespacing*{\subsection}{0pt}{4pt}{0pt}
\titleformat{\subsection} % TODO: Change how it looks
	{\normalsize\headerfont\bf\raggedright}
	{}{0em}
  	{}
  
\newcommand{\datedsection}[2]{%
  \section[#1]{#1 \hfill #2}%
}

\newcommand{\datedsubsection}[2]{%
  \subsection[#1]{#1 \hfill \textnormal{#2}}%
}

% Link specific commands
\newcommand{\githublink}[1]{\href{https://github.com/#1}{\texttt{GitHub.com/#1}}}


