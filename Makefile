# LaTeX Makefile
# Author: Anand Balakrishnan
# Email: anandbala1597@gmail.com

export TEXINPUTS:=.:./texmf:${TEXINPUTS}
BUILD_DIR			= _build
COMPILER			= xelatex
PROJECT				= AnandBalakrishnanResume

ifeq (${CI},true)
	TAG	= -${BITBUCKET_TAG}
else
	TAG	= 
endif


pdf: $(BUILD_DIR)/main.pdf

$(BUILD_DIR)/main.pdf: main.tex two-col-cv.cls
	@echo "Building $(PROJECT) in $(BUILD_DIR) using $(COMPILER)"
	@echo "Creating $(BUILD_DIR)"
	@mkdir -p $(BUILD_DIR)
	$(COMPILER) -interaction=nonstopmode -halt-on-error -output-directory=$(BUILD_DIR) main.tex
	@echo "First pass (via $(COMPILER)) done!"
	$(COMPILER) -interaction=nonstopmode -halt-on-error -output-directory=$(BUILD_DIR) main.tex
	@echo "Second pass (via $(COMPILER)) done!"
	$(COMPILER) -interaction=nonstopmode -halt-on-error -output-directory=$(BUILD_DIR) main.tex
	@echo "Third pass (via $(COMPILER)) done!"


deploy: pdf
	@cp $(BUILD_DIR)/main.pdf $(BUILD_DIR)/$(PROJECT)-latest.pdf
	@cp $(BUILD_DIR)/main.pdf $(BUILD_DIR)/$(PROJECT)$(TAG).pdf
	@echo "Deploying $(BUILD_DIR)/$(PROJECT)-latest.pdf to Dropbox"
	curl -X POST https://content.dropboxapi.com/2/files/upload \
    --header 'Authorization: Bearer $(DROPBOX_ACCESS_TOKEN)' \
    --header 'Dropbox-API-Arg: {"path": "/$(PROJECT)-latest.pdf","mode": "overwrite"}' \
    --header "Content-Type: application/octet-stream" \
    --data-binary @'$(BUILD_DIR)/$(PROJECT)-latest.pdf'
	@echo "Deploying $(BUILD_DIR)/$(PROJECT)$(TAG).pdf to Dropbox"
	curl -X POST https://content.dropboxapi.com/2/files/upload \
    --header "Authorization: Bearer $(DROPBOX_ACCESS_TOKEN)" \
    --header 'Dropbox-API-Arg: {"path": "/$(PROJECT)$(TAG).pdf","mode": "overwrite"}' \
    --header "Content-Type: application/octet-stream" \
    --data-binary @'$(BUILD_DIR)/$(PROJECT)$(TAG).pdf'





